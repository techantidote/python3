#!/usr/bin/python3

## Define a Lists 
tags = ["technology", "python", "linux"]
print ("tags = ", tags)

# Read specific range of elements - first 2 elements
print ("First element", tags[0])
print ("First two elements =" , tags[:2])
print ("Last  element", tags[:-1])

print (" End ")

