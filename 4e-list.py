#!/usr/bin/python3

## Define a Lists 
tags = ["technology", "python", "linux"]
print ("tags => ", tags, "Lenght ", len(tags))

# Define another list
new_tags = ["redhat", "engineering", "random"]
print ("new_tags => ", new_tags)


# Adding elements of a list to another list using extend
tags.extend(new_tags)
print("tags =>", tags, "Lenght", len(tags))

