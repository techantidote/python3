#!/usr/bin/python3

# This does not escape \n
print ("C:\newfoder")

# To treat whole as literal string
print (r"C:\newfoder")


## String operations
first = "Tech "
last = "Antidote"

full_name = first + last
print (full_name)

# Print multiple times
print (full_name * 2)

## Other Shiz

website = "https://techantidote.com"
## Accessing single character in a position
print (website[0])
# Last character
print (website[-1])

# Print first x charaters of string - It will not print the 8th element
print (website[:8])

# Print last 3 charaters of string
print (website[-3:])

# Print subset
print ("FQDN = ", website[8:])

# Upper case
web_upper = website.upper()
print ("Upper case = ", web_upper)

# Range of characters
print (website[8:-4])

# Get Lenght of string

str_len = len(website)
print ("Length of website =", str_len)
