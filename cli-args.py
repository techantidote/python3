#!/usr/bin/python3
# CLI args # Tested on 3.8.10

from argparse import ArgumentParser
import pathlib
import sys

# hack for py3 fstrings
new_line="\n"

parser = ArgumentParser()
parser.add_argument("-i", "--inputfile", dest="inputfile", type=pathlib.Path, help="Input File", metavar="FILE")
parser.add_argument("-o", "--outputfile", dest="outputfile", type=pathlib.Path, help="Output File", metavar="FILE")
parser.add_argument("-c", "--count", dest="count", type=int, help="count shiz",metavar="COUNT")
parser.add_argument("-d", "--debug",action="store_true", dest="debug", default=False, help="Enable Debugging")

args = parser.parse_args()
#print(args)

# Use this to handle no arguments using sys.argv
if len(sys.argv) < 2:
    parser.print_usage()
#    parser.print_help()
    sys.exit(1)

if(args.outputfile):
    print(f'Output File: {args.outputfile}')

if(args.inputfile):
    print(f'Input File: {args.inputfile}')

if(args.count):
    print(f'Count: {args.count}')

if(args.debug):
    print(f'Debug: {args.debug} | Debugging is Enabled')
