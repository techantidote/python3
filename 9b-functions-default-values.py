#!/usr/bin/python3

## Here, we are specifying a defualt value
def get_log_name(log_file='default-log.txt'):
    print ("Log File: ",log_file)

# Passing no arguments in below function. i.e. not specifying log_file name
get_log_name()

# Now, pass parameter "sample.txt" to funtion and this value will be used
get_log_name("sample.txt")
