#!/usr/bin/python3

x = 10
y = 2
z = 3

print ("x =",x,"y =",y,"z=",z)
print (x / y)
print (x / z)

## Round the number to nearest decimal
print (x // z)

# Get remainder of division
division_result = x / z
print ("Result = ",division_result);

rem = x % z
print ("Remainder = ",rem)

### x^y (X to the power of Y)
print ("x to the power y = ", x ** y)
