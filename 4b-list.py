#!/usr/bin/python3

## Define a Lists 
tags = ["technology", "python", "linux"]
print ("tags = ", tags)


# To change value of an element
tags[0] = "New Technology"
# Print updated tags and lenght
print ("Updated Tags = ", tags, " Lenght = ", len(tags))

# To delete a specfic element - Set it to empty
print (r"Deleting 1st element of list =>  ", tags)
tags[0] = [] 
print ("Updated list after deletion =", tags, "Lenght = ", len(tags))
print ("END")

