#!/usr/bin/python3

# Set default header as Content-Type: application/x-www-form-urlencoded
def get_header_type(header="Content-Type: application/x-www-form-urlencoded"):

    if header == "json":
        header = "Content-Tyype application/json"

    if header == "force":
        header = "Content-Type application/force-download"

    if header == "Content-Type: application/x-www-form-urlencoded":
        print("Using default header")

    return header

header = get_header_type()
print ("Header: ", header)
header = get_header_type("json")
print ("Header: ", header)
header = get_header_type("force")
print ("Header: ", header)
