#!/usr/bin/python3

tags = ["Technology", "Linux", "Random", "Redhat", "Kali"]
print (" tags => ", tags)

for item in tags:
    print (item)

## Loop only upto a certain position. 
## Example: Loop only till first two elements - excluding third

print ("==Running loop only upto 2nd position in list==")
for item in tags[:2]:
    print (item)
