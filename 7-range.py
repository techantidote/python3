#!/usr/bin/python3

# range example

print (r"1st example with range(5)")
for x in range(5):
    print (x)

# Range 
print (r"2nd example with range(5,10)")
for x in range(5,10):
    print (x)


print (r"3nd example with range(5,40,5)")
for x in range(5,40,5):
    print (x)
