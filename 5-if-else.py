#!/usr/bin/python3

website = "Techantidote.com"
author = "Techantidote"

if website is "Techantidote":
    print("True")
elif website is "Techantidote.com":
    print ("Its Techantidote.com")

# Alternative way
if author == "random":
    print ("False")
elif author == "Techantidote":
    print ("Author is", author)

# Else statement
email = "test.com"
if email == "random":
    print ("False")
elif email == "Techantidote":
    print ("email is: ", email)
else:
    print ("email Not found")

