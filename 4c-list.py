#!/usr/bin/python3

## Define a Lists 
tags = ["technology", "python", "linux"]
print ("tags => ", tags, "Lenght ", len(tags))

## Add data to list
print(" Add data to list")
tags.append("personal")
print ("Updated list => ", tags, "Lenght = ", len(tags))

# Define another list
new_tags = ["redhat", "engineering", "random"]
print ("new_tags => ", new_tags)

# A way to append but not affect the original values
print ("tags list + new_tags = ", tags + new_tags)
# Printing tags will only show original value
print("tag list", tags, "Lenght =", len(tags))

# Alternatively, store the appended value to a list
appended_list = tags + new_tags
print ("appended_list =>  ", appended_list);
print ("Note: Check the length")

print("Here is list 'tags' and lenght");
print("tags => ", tags, "Lenght", len(tags))
print("Here is list 'appended_list' and lenght")
print("appended_list => ", appended_list, "Lenght", len(appended_list))
print("Note: Here the original list is still the same. But the appended_list has the new list")

